module.exports = {
    filenameHashing: false,
    devServer: {
      proxy: {
        '^/api': {
          /*target: 'https://weathersound.fr',*/
          target: 'http://localhost:8000',
          changeOrigin: true
        },
      }
    }
  }
